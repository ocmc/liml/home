---
title: "Home"
---

![OCMC Logo](/images/limlOcmc.png)

Below is a list of websites that provide Digital Chant Stands (DCS) with liturgical books, daily services, musical scores, and audio recordings in various languages.  These websites were produced using software created by OCMC and reusable templates created by Fr. Seraphim Dedes. Additional languages will be added in the coming months and years.

#### Digital Chant Stands Currently Available

| Website | Primary Language | Additional Languages  | Jurisdiction |
| ----------- | ----------- | ----------- | ----------- |
| [dcs.goarch.org](https://dcs.goarch.org) | English | Greek | Greek Orthodox Archdiocese of America (Ecumenical Patriarchate) |
| [liturgia.mx](https://liturgia.mx)   | Spanish   | English, Greek | Greek Orthodox Archbishopric of Mexico (Ecumenical Patriarchate)      |
| [demo.liml.org](https://demo.liml.org) | English | Greek | Demonstration of the GOA DCS generated using Doxa      |
| [eac.liml.org](https://eac.liml.org) | Swahili   | English, Greek | East African Community Archdioceses and Bishoprics (Greek Orthodox Patriarchate of Alexandria and All Africa)       |
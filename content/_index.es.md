---
title: "Inicio"
---

![OCMC Logo](/images/limlOcmc.png)

## La Liturgia en _Mi_ Idioma

A continuación se puede ver una lista de sitios web que proporcionan los recursos de un Analogion Digital (Digital Chant Stand): libros litúrgicos, sacramentos y servicios completos, partituras musicales y grabaciones de audio en varios idiomas. Estos sitios fueron desarrollados utilizando software creado por la agencia misionera OCMC (Orthodox Christin Mission Center) y plantillas reutilizables creadas por el Padre Seraphim Dedes. Más idiomas serán agregados en los próximos años.

#### Sitios web del tipo "Analogion Digital"

| Sitio Web | Idioma Principal | Idiomas Adicionales  | Jurisdicción Eclesiástica |
| ----------- | ----------- | ----------- | ----------- |
| [dcs.goarch.org](https://dcs.goarch.org) | inglés | griego | Arquidiócesis Griega Ortodoxa de Norteamérica (Patriarcado Ecuménico) |
| [liturgia.mx](https://liturgia.mx)   | español   | inglés, griego | Sacro Arzobispado Ortodoxo Griego de México (Patriarcado Ecuménico)      |
| [demo.liml.org](https://demo.liml.org) | inglés | griego | Demostración del sitio web GOA DCS generado con Doxa      |
| [eac.liml.org](https://eac.liml.org) | swahili   | inglés, griego | Conjunto de los obispados y arquidiócesis de África oriental (Patriarcado Ortodoxo Griego de Alejandría y África)       |
---
title: "Home"
---

![OCMC Logo](/images/limlOcmc.png)

Nenda ici ciĩ haha kĩanda cirahe aini (Chant Stands) cia kĩ-rĩu iria ciĩ na mabuku ma igongona, nyĩmbo nyandĩke hamwe na mũinire wa cio na thiomi-inĩ itiganĩte. Nenda ici ciumanĩte na mĩtambo ĩrĩa ĩthondeketwo nĩ OCMC hamwe na mĩbangĩre ya Igongona ya nenda-inĩ ĩrĩa ĩthondeketwo nĩ Fr. Seraphim Dedes. Thiomi ĩngĩ itiganĩte nĩ cikuongererwo matukũ-inĩ ma thutha.

#### Ici nĩ (Chant Stands) cia kĩ-rĩu iria ciĩ kuo

| Rũrenda | Rũthiomi rwa Mbere | Thiomi ndiganu  | Kanitha wa... |
| ----------- | ----------- | ----------- | ----------- |
| [dcs.goarch.org](https://dcs.goarch.org) | Gĩthũngũ | Kĩ-Ngiriki| Kanitha wa Ngiriki wa Orthodox wa Amerika (Ecumenical Patriarchate) |
| [liturgia.mx](https://liturgia.mx)   | Ki-Hispania   | Gĩthũngũ, Kĩ-Ngiriki | Kanitha wa Ngiriki wa Orthodox wa Mexico (Ecumenical Patriarchate)      |
| [demo.liml.org](https://demo.liml.org) | Gĩthũngũ | Kĩ-Ngiriki | Kĩonereria kĩa GOA DCS ĩrĩa ĩthondeketwo nĩ Doxa      |
| [eac.liml.org](https://eac.liml.org) | Gĩthwaĩri   | Gĩthũngũ, Kĩ-Ngiriki | Kanitha cia Orthodox cia Afrika ya Irathĩro (Patriakĩti ya Kĩ-Ngiriki ya Orthodox ya Alexandria na Afrika Yoothe)       |
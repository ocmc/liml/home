---
title: "Inicio"
---

![OCMC Logo](/images/limlOcmc.png)

## Liturghia kwa Lugha _Yangu_

Orodha ifuatayo ni ya tovuti (websaiti) zinazotwaa Stendi za Dijitali za Nyimbo (SDN) zikiwa na vitabu vya ibaada, ibaada zenyewe, manoti ya muziki, na sauti za kurekodiwa  katika lugha mbalimbali. Tovuti hizi zimetengenezwa kwa kutumia programu iliyoundwa na OCMC pamoja na kutumia violezo vinavyoweza kutumika tena vilivyoundwa na Fr Serafim Dedes. Lugha zaidi zitaongezewa baadaye.

#### Stendi za Dijitali za Nyimbo Zinazopatikana kwa Sasa

| Tovuti (Websaiti) | Lugha Kuu | Lugha Zingine  | Jimbo |
| ----------- | ----------- | ----------- | ----------- |
| [dcs.goarch.org](https://dcs.goarch.org) | Kingereza | Kigiriki | Kanisa la Kiorthodoksi la Kigiriki la Marekani (Patriaketi ya Kiekumeniki) |
| [liturgia.mx](https://liturgia.mx)   | Kihispania   | Kingereza, Kigiriki | Kanisa la Kiorthodoksi la Kigiriki la Mexico (Patriaketi ya Kiekumeniki)      |
| [demo.liml.org](https://demo.liml.org) | Kingereza | Kigiriki | Maonyesho ya tovuti ya GOA DCS iliyotengenezwa kwa kutumia Doxa      |
| [eac.liml.org](https://eac.liml.org) | Kiswahili   | Kingereza, Kigiriki | Jumuiya ya Afrika Mashariki - Dayosisi kuu lizizo chini ya Patriaketi ya Orthodoksi ya Kigiriki ya Aleksandria na Afrika Yote       |